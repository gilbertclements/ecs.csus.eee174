


con

  _clkmode = xtal1 + pll16x
  _xinfreq = 5_000_000                                          ' use 5MHz crystal

  CLK_FREQ = ((_clkmode - xtal1) >> 6) * _xinfreq
  MS_001   = CLK_FREQ / 1_000
  US_001   = CLK_FREQ / 1_000_000


con

  RX1     = 31                                                  ' programming / terminal
  TX1     = 30
  
  SDA     = 29                                                  ' eeprom / i2c
  SCL     = 28

  LCD_SDA = 14                                                  ' okay with 5v I2c pins on LCD
  LCD_SCL = 15


con

  LCD_WIDTH = 16
  

obj

  lcd : "jm_twi_lcd"


var


dat

  mouth0        byte    $0E, $1F, $1C, $18, $1C, $1F, $0E, $00
  mouth1        byte    $0E, $1F, $1F, $18, $1F, $1F, $0E, $00
  mouth2        byte    $0E, $1F, $1F, $1F, $1F, $1F, $0E, $00
  smile         byte    $00, $0A, $0A, $00, $11, $0E, $06, $00

 '' banner1       byte    $20, $20, $20, $20, $20, $20, $20, $20
 ''               byte    $20, $20, $20, $20, $20, $20, $20, $20
  ''              byte     $50, $61, $72, $6b, $69, $6e, $67, $20, $53, $70, $6f, $74, $73, $0a
              

  banner1       byte    "  Lvl 1   Lvl 2  ", 0 

  banner2       byte    "Full    Empty  ", 0 


pub main | idx, pos, frame, char, newchar, tf, tc

  lcd.start(LCD_SCL, LCD_SDA, 7)                                ' start i2c lcd

  

  repeat
    lcd.cmd(lcd#LCD_CLS)
    pause(250)

    ' scroll on 1st line
    
    
    lcd.move_cursor(0, 0)
      lcd.sub_str(@banner1, 1, LCD_WIDTH)
      pause(75)

    ' animate 2nd line
    lcd.move_cursor(1, 1)
    lcd.sub_str(@banner2,0,LCD_WIDTH)
    
  '  repeat pos from 0 to 15                                     ' scroll through all chars
  '    char := byte[@banner2]                               ' get char from banner2
   '   repeat frame from 1 to 5                                  ' loop through animation frames
  '      lcd.move_cursor(pos, 1)                                 ' position cursor
  '      newchar := lookup(frame : 0, 1, 2, 1, char)             ' get char for frame
   '     lcd.out($30)                                        ' write it
  '      pause(75)                                               ' short, inter-frame delay
         
    pause(2000)                                                 ' hold for 2 seconds

    
    ' numeric formatting demo

    lcd.cmd(lcd#LCD_CLS)
     

      
    
con

  { --------------- }
  {                 }
  {   B A S I C S   }
  {                 }
  { --------------- }


pub pause(ms) | t

'' Delay program in milliseconds

  if (ms < 1)                                                   ' delay must be > 0
    return
  else
    t := cnt - 1776                                             ' sync with system counter
    repeat ms                                                   ' run delay
      waitcnt(t += MS_001)
    

pub high(pin)

'' Makes pin output and high

  outa[pin] := 1
  dira[pin] := 1


pub low(pin)

'' Makes pin output and low

  outa[pin] := 0
  dira[pin] := 1


pub toggle(pin)

'' Toggles pin state

  !outa[pin]
  dira[pin] := 1


pub input(pin)

'' Makes pin input and returns current state

  dira[pin] := 0

  return ina[pin]
  
        
dat


 